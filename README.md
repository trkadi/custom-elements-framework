# README

This package is a UI library for Spotfire.

## Components

1. **CSS library (like Bootstrap's)**

   _Contains classes, media queries, rules, etc. (not included in this repository)_

2. **Custom web component library for complex elements**

   _Contains standalone custom elements like \<shiny-header\> and so.._

```html
<!-- With web components -->
<shiny-header>
  <shiny-breadcrumb slot="slot1"></shiny-breadcrumb>
  <shiny-controls slot="slot2"></shiny-controls>
  <div class="rest-content">Something cool!</div>
  <div class="rest-content">Something other cool!</div>
</shiny-header>

<!-- Without web components -->
<div class="shiny-header">
  <div class="shiny-breadcrumb">...</div>
  <div class="shiny-controls">...</div>
</div>
```

## Library serving

We see a few possibilities for serving the components:

1. **IDEA #1**: _Containerized nginx http server besides Spotfire_
2. **IDEA #2**: _C# plugin for spotfire to serve static content?_
3. **IDEA #3**: _Put the necessary code in spotfire_

### Dockerized Nginx HTTP server

This server is needed for serving custom static content for the Spotfire environment (things to show in spotfire dashboards..).

#### Setup

1. SSH into the machine on which the Spotfire server is running.
2. Check for docker with `docker` command
3. Create the necessary folders, for example:
   1. `mdkir -p /opt/tibco/tss/<VERSION_OF_SPOTFIRE>/tomcat/custom-static-content/`
4. Run a container in docker with the next command, for example:
   1. `docker container run -p 8080:80 -v /opt/tibco/tss/<VERSION_OF_SPOTFIRE>/tomcat/custom-static-content:/usr/share/nginx/html -d nginx:<WANTED-VERSION>-alpine`
5. Try the running nginx HTTP server by opening a browser on the terminal and open the proper URL.
   1. `http://<public-url-of-the-server>:8080/`

**NOTE**: _CORS_ setting is important!

#### Custom static content

You can populate the previously created folder on the Spotfire server's machine with any static content.

## Working principle

- There is a central file (index.js) which should be included into the first HTML occurence on a Spotfire dashboard by a script tag as a module. Because this file is an ES6 module you don't have to include the other files, that is fully automated.

## Known things to be implemented

1. In the template and in the CSS the paths are relative to the ROOT! This means that any relative URL in the template is chained after the base URL!
   1. We have a fix for specific scenarios. **Relative** URLs are replaced. URLs with `http://`, `https://`, `data:` are left intact!
      1. HTML: `src="..."`
      2. CSS: `url('...')`
