import { BaseComponent } from "../BaseComponent.js";

// 'shiny-header'
const tagName = import.meta.url.match(/[^\/]+(?=\.js$)/)[0];

export class ShinyHeader extends BaseComponent {
  static async getTemplate() {
    await super.getTemplate(tagName);
  }

  static async getStyle() {
    await super.getStyle(tagName);
  }

  /**
   * ^--- Constructor class functions
   * ------------------------------------
   * v--- Instance functions
   */

  constructor() {
    super();
  }
}
