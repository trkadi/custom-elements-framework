export class CustomComponentLoader {
  static async loadComponents(componentMap) {
    componentMap.forEach(async (componentTag) => {
      // shiny-dropdown -> ShinyDropdown
      const className = componentTag.replace(/(^.)|(-.)/g, (a) => {
        if (a.length === 1) return a.toUpperCase();
        else return a[1].toUpperCase();
      });

      const module = await import(`./${componentTag}/${componentTag}.js`);

      /**
       * NOTE: Here we tell the component's class to get it's template
       * or in other words cache the template in it's static field.
       * Maybe this could be in the components somehow..?
       */
      await module[className].getTemplate();
      await module[className].getStyle();

      customElements.define(componentTag, module[className]);
    });
  }
}
