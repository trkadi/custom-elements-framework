const __dirname = import.meta.url.match(/.+(?=\/.+$)/)[0];

export class BaseComponent extends HTMLElement {
  static template = "";
  static style = "";

  static async getTemplate(tagName) {
    this.template = await fetch(`${__dirname}/${tagName}/${tagName}.html`)
      .then((response) => response.text())
      .then((tpl) =>
        tpl.replace(
          /(src=")((?!https?:\/\/|data:).+?)(")/g,
          (...matchObject) => {
            return `${matchObject[1]}${__dirname}/${tagName}/${matchObject[2]}${matchObject[3]}`;
          }
        )
      )
      .catch((rejectReason) => console.error(rejectReason));
  }

  static async getStyle(tagName) {
    this.style = await fetch(`${__dirname}/${tagName}/${tagName}.css`)
      .then((response) => response.text())
      .then((style) =>
        style.replace(
          /(url\(['"])((?!https?:\/\/|data:).+?)(['"]\))/g,
          (...matchObject) => {
            console.log(
              `${matchObject[1]}${__dirname}/${tagName}/${matchObject[2]}${matchObject[3]}`
            );

            return `${matchObject[1]}${__dirname}/${tagName}/${matchObject[2]}${matchObject[3]}`;
          }
        )
      )
      .catch((rejectReason) => console.error(rejectReason));
  }

  /**
   * ^--- Constructor class functions
   * ------------------------------------
   * v--- Instance functions
   */

  constructor() {
    super();
    this.createInnerContent();
  }

  async createInnerContent() {
    const shadowRoot = this.attachShadow({ mode: "open" });
    const styleElement = document.createElement("style");

    shadowRoot.innerHTML = this.constructor.template;
    styleElement.innerHTML = this.constructor.style;

    shadowRoot.append(styleElement);
  }
}
