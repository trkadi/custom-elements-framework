import { CustomComponentLoader } from "./components/component-loader.js";

/**
 * Because this is not a NodeJS environment
 * where we can loop through a directory
 * we need to list the used components
 * explicitly.
 */
CustomComponentLoader.loadComponents(["shiny-header"]);
